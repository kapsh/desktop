# Copyright 2013 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2013 Marc-Antoine Perennou <keruspe@exherbo.org>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gitlab [ prefix=https://gitlab.freedesktop.org ] \
    meson \
    systemd-service \
    vala [ vala_dep=true with_opt=true option_name=gobject-introspection ]

SUMMARY="GeoClue location framework"
DESCRIPTION="
A software framework which can be used to enable geospatial awareness in applications.  GeoClue uses
DBus to provide location information.
"

LICENCES="GPL-2"
SLOT="2.0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    avahi [[ description = [ Enable support for NMEA ] ]]
    gobject-introspection
    gtk-doc
    modem-manager [[ description = [ Build 3G, CDMA and modem GPS backend ] ]]
"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3]
        sys-devel/gettext
        virtual/pkg-config[>=0.22]
        gtk-doc? ( dev-doc/gtk-doc[>=1.0] )
    build+run:
        core/json-glib[>=0.14.0]
        dev-libs/glib:2[>=2.44.0]
        gnome-desktop/libsoup:2.4[>=2.42.0]
        avahi? ( net-dns/avahi[>=0.6.10] )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.6] )
        modem-manager? ( net-wireless/ModemManager[>=1.6] )
"

MESON_SOURCE=${WORKBASE}/${PNV}-9b6c03640e66a4e7d615200d95b89051b2214da1

MESON_SRC_CONFIGURE_PARAMS=(
    -Ddbus-sys-dir=/usr/share/dbus-1/system.d
    -Ddemo-agent=false
    -Denable-backend=true
    -Dlibgeoclue=true
    -Dsystemd-system-unit-dir=${SYSTEMDSYSTEMUNITDIR}
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'avahi nmea-source'
    'gobject-introspection introspection'
    gtk-doc
    'modem-manager 3g-source'
    'modem-manager cdma-source'
    'modem-manager modem-gps-source'
)

