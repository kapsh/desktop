# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require test-dbus-daemon option-renames [ renames=[ 'gnutls providers:gnutls' ] ]
require utf8-locale

SUMMARY="Jabber Plugin for Telepathy"
HOMEPAGE="https://telepathy.freedesktop.org/"
DOWNLOADS="https://telepathy.freedesktop.org/releases/${PN}/${PNV}.tar.gz"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="debug gtk-doc
    ( providers: gnutls libressl openssl ) [[ number-selected = exactly-one ]]
"

# DBusException: org.freedesktop.DBus.Error.NoReply: Message did not receive a reply (timeout by
# message bus) appears on some systems
# Needs 0.0.0.0 whitelisting
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=2.5]
        dev-libs/libxslt
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc[>=1.17] )
    build+run:
        dev-db/sqlite:3
        dev-libs/dbus-glib:1[>=0.82]
        dev-libs/glib:2[>=2.32]
        dev-libs/libxml2:2.0
        gnome-desktop/libsoup:2.4
        net-im/libnice[>=0.0.11]
        net-im/telepathy-glib[>=0.19.9]
        sys-apps/dbus[>=1.1.0]
        providers:gnutls? ( dev-libs/gnutls[>=2.12] )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
    suggestion:
        (
            gnome-desktop/gobject-introspection:*
            x11-libs/gtk+:*[gobject-introspection]
            gnome-desktop/gtksourceview:*[gobject-introspection]
        ) [[ *description = [ used by telepathy-gabble-xmpp-console ]  *group-name = [ xmpp-console ] ]]
    test:
        dev-python/dbus-python
        dev-python/pyopenssl
        net-libs/cyrus-sasl[>=2] [[ note = [ for wocky ] ]]
        net-twisted/Twisted
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PNV}-Update-Python-tools-from-telepathy-glib-avoid-deprec.patch
    "${FILES}"/${PN}-0.18.4-openssl.patch
)

pkg_setup() {
    # python 3.7 forces an UTF-8 locale so the line below can be dropped if
    # it's our oldest python3 ABI.
    require_utf8_locale
}

src_configure() {
    econf --with-ca-certificates=/etc/ssl/certs/ca-certificates.crt \
          --with-tls=$(optionv providers:gnutls || echo openssl)    \
          $(option_enable debug)                                    \
          $(option_enable providers:gnutls prefer-stream-ciphers)   \
          $(option_enable gtk-doc)                                  \
          --disable-static
}

src_test() {
    esandbox allow_net "unix:${TEMP%/}/dbus-gabble-*"

    test-dbus-daemon_run-tests

    esandbox disallow_net "unix:${TEMP%/}/dbus-gabble-*"
}

