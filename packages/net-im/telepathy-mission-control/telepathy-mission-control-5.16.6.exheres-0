# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 1.13 ] ] \
        python [ multibuild=false blacklist=none ] \
        gsettings \
        test-dbus-daemon

SUMMARY="Abstraction over details of connection managers"
HOMEPAGE="https://telepathy.freedesktop.org"
DOWNLOADS="${HOMEPAGE}/releases/${PN}/${PNV}.tar.gz"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    debug
    gtk-doc
    networkmanager [[ description = [ Connectivity support via NetworkManager ] ]]"

# needs dconf
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-libs/libxslt
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc[>=1.17] )
    build+run:
        dev-libs/dbus-glib:1[>=0.82]
        dev-libs/glib:2[>=2.46]
        net-im/telepathy-glib[>=0.19.0]
        sys-apps/dbus[>=0.95]
        networkmanager? ( net-apps/NetworkManager )
    test:
        dev-python/dbus-python
        net-twisted/Twisted
"

AT_M4DIR=( m4 )

src_prepare() {
    # Install dbus service files into the correct location
    edo sed -e '/servicefiledir/s:=.*:= /usr/share/dbus-1/services:' \
            -i server/Makefile.am

    autotools_src_prepare
}

src_configure() {
    econf --enable-conn-setting \
          --disable-upower \
          --disable-static \
          $(option_enable debug ) \
          $(option_enable gtk-doc ) \
          $(if option networkmanager; then
                echo '--with-connectivity=nm'
            else
                echo '--without-connectivity'
            fi)
}

src_install() {
    gsettings_src_install
    keepdir /usr/$(exhost --target)/lib/mission-control-plugins.${SLOT}
}

