# Copyright 2017-2019 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=storaged-project release=${PV}-1 suffix=tar.gz ]

SUMMARY="A library for low-level manipulation with block devices"
DESCRIPTION="
libblockdev is a C library supporting GObject introspection for manipulation of block devices. It
has a plugin-based architecture where each technology (like LVM, Btrfs, MD RAID, Swap,...) is
implemented in a separate plugin, possibly with multiple implementations (e.g. using LVM CLI or the
new LVM DBus API).
"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS="
    btrfs [[ description = [ Support for Btrfs filesystems ] ]]
    cryptsetup [[ description = [ Support for LUKS encrypted volumes ] ]]
    gobject-introspection
    gtk-doc
    lvm [[ description = [ Support for LVM volumes ] ]]
    mdraid [[ description = [ Support for software RAID (MD-RAID) ] ]]
    mpath [[ description = [ Support for multipath devices ] ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
        gtk-doc? ( dev-doc/gtk-doc )
    build+run:
        dev-libs/glib:2[>=2.42.2]
        dev-libs/libbytesize[>=0.1]
        sys-apps/kmod[>=19]
        sys-apps/util-linux[>=2.30.0] [[ note = [ libblkid and libmount ] ]]
        sys-fs/parted[>=3.2]
        cryptsetup? (
            app-crypt/volume_key
            dev-libs/nss[>=3.18.0]
            sys-fs/cryptsetup[>=2.3.0] [[ note = [ automagic cryptsetup 2+ ] ]]
        )
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.3.0] )
        lvm? ( sys-fs/lvm2[>=1.02.93] )
        mpath? ( sys-fs/lvm2[>=1.02.93] )
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd[>=216] )
    run:
        sys-apps/gptfdisk[>=0.8.6]
        btrfs? ( sys-fs/btrfs-progs[>=3.18.2] )
        mdraid? ( sys-fs/mdadm[>=3.3.2] )
        mpath? ( sys-fs/multipath-tools )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --with-bcache
    --with-escrow
    --with-fs
    --with-kbd
    --with-loop
    --with-part
    --with-swap
    --without-dm
    --without-dmraid
    --without-nvdimm
    --without-python2
    --without-python3
    --without-vdo
    --without-s390
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
   'gobject-introspection introspection'
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    btrfs
    'cryptsetup crypto'
    gtk-doc
    lvm
    'lvm lvm_dbus'
    'lvm tools'
    mdraid
    mpath
)
DEFAULT_SRC_CONFIGURE_TESTS=( '--enable-tests --disable-tests' )

