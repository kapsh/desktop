# Copyright 2015-2016 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=masmu tag=${PV} ] setup-py [ import=setuptools blacklist=3 ] systemd-service

SUMMARY="Small DLNA server which brings DLNA/UPNP and Chromecast support to PulseAudio and Linux"
DESCRIPTION="
It can stream your current PulseAudio playback to different UPNP devices (UPNP Media Renderers) or
Chromecasts in your network. It's main goals are: easy to use, no configuration hassle, no big
dependencies. UPNP renderers in your network will show up as pulseaudio sinks.
"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        app-misc/ca-certificates
        dev-python/pip[python_abis:*(-)?]
        dev-python/virtualenv[python_abis:*(-)?]
        sys-apps/help2man
    build+run:
        dev-python/chardet[>=2.0.1][python_abis:*(-)?]
        dev-python/dbus-python[>=1.2.0][python_abis:*(-)?]
        dev-python/docopt[>=0.6.2][python_abis:*(-)?]
        dev-python/lxml[>=3][python_abis:*(-)?]
        dev-python/netifaces[>=0.8][python_abis:*(-)?]
        dev-python/notify2[>=0.3][python_abis:*(-)?]
        dev-python/protobuf[>=2.5.0][python_abis:*(-)?]
        dev-python/psutil[>=1.2.1][python_abis:*(-)?]
        dev-python/requests[>=2.2.1][python_abis:*(-)?]
        dev-python/setproctitle[>=1.0.1][python_abis:*(-)?]
        dev-python/zeroconf[>=0.17][python_abis:*(-)?]
        gnome-bindings/pygobject:3[>=3.12.0][python_abis:*(-)?]
        python_abis:2.7? (
            dev-python/futures[>=2.1.6][python_abis:*(-)?]
        )
    run:
        media-sound/pulseaudio
    recommendation:
        media-libs/faac[>=1.28] [[
            description = [ Advanced Audio Coding (aac) support ]
        ]]
        media-libs/flac[>=1.3.1] [[
            description = [ Free Lossless Audio Codec (flac) support ]
        ]]
        media-sound/lame[>=3.99.0] [[
            description = [ MPEG Audio Layer III (mp3) support ]
        ]]
        media-sound/opus-tools[>=0.1.8] [[
            description = [ Opus Interactive Audio Codec (opus) support ]
        ]]
        media-sound/sox[>=14.4.1] [[
            description = [ Linear PCM (l16) and Waveform Audio File Format (wav) support ]
        ]]
        media-sound/vorbis-tools[>=1.4.0] [[
            description = [ Ogg Vorbis (ogg) support ]
        ]]
    suggestion:
        gnome-bindings/pygobject:2 [[
            description = [ Optional cover mode suppport ]
        ]]
        media-sound/pavucontrol [[
            description = [ Provides an easy way to route audio to different pulseaudio sinks ]
        ]]
        sys-apps/iproute2 [[
            note = [ The ip command is used internally to determine the IP address when systemd-networkd isn't used ]
        ]]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/d9051b483435f4c21c3048b0702defcb02dc1025.patch
)

src_install() {
    setup-py_src_install

    install_systemd_files
}

