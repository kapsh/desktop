# Copyright 2018-2020 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=KhronosGroup tag=051c6fed884b6837dc5728adc9e0ba25ba8f4212 ] cmake

SUMMARY="Khronos reference front-end for GLSL and ESSL, and sample SPIR-V generator"
DESCRIPTION="
An OpenGL and OpenGL ES shader front end and validator. There are several components:
* A GLSL/ESSL front-end for reference validation and translation of GLSL/ESSL into an AST.
* An HLSL front-end for translation of a broad generic HLL into the AST. See issue 362 and issue
  701 for current status.
* A SPIR-V back end for translating the AST to SPIR-V.
* A standalone wrapper, glslangValidator, that can be used as a command-line tool for the above.
"
HOMEPAGE+=" https://www.khronos.org/opengles/sdk/tools/Reference-Compiler"

LICENCES="Apache-2.0 BSD-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

# glslang-testsuite fails, last checked: 8.13.3743
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_BUILD_TYPE:STRING=Release
    -DBUILD_EXTERNAL:BOOL=TRUE
    -DBUILD_SHARED_LIBS:BOOL=FALSE
    -DENABLE_EMSCRIPTEN_ENVIRONMENT_NODE:BOOL=FALSE
    -DENABLE_EMSCRIPTEN_SINGLE_FILE:BOOL=FALSE
    -DENABLE_GLSLANG_BINARIES:BOOL=TRUE
    -DENABLE_GLSLANG_JS:BOOL=FALSE
    -DENABLE_GLSLANG_WEBMIN:BOOL=FALSE
    -DENABLE_GLSLANG_WEBMIN_DEVEL:BOOL=FALSE
    -DENABLE_HLSL:BOOL=TRUE
    -DENABLE_OPT:BOOL=TRUE
    -DENABLE_PCH:BOOL=TRUE
    -DENABLE_RTTI:BOOL=FALSE
    -DENABLE_SPVREMAPPER:BOOL=TRUE
    -DSKIP_GLSLANG_INSTALL:BOOL=FALSE
    -DUSE_CCACHE:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
    '-DENABLE_CTEST:BOOL=TRUE -DENABLE_CTEST:BOOL=FALSE'
)

