# Copyright 2017 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SUMMARY="Unicode emoji"
DESCRIPTION="
Emoji are pictographs (pictorial symbols) that are typically presented in a colorful form and used
inline in text. They represent things such as faces, weather, vehicles and buildings, food and
drink, animals and plants, or icons that represent emotions, feelings, or activities.
"
HOMEPAGE="https://www.unicode.org/emoji/"

EMOJI_FILES=(
    ReadMe.txt
    emoji-data.txt
    emoji-sequences.txt
    emoji-test.txt
    emoji-variation-sequences.txt
    emoji-zwj-sequences.txt
)

DOWNLOADS="
    $(for f in "${EMOJI_FILES[@]}"; do
        echo "https://www.unicode.org/Public/emoji/${PV}/${f} -> ${PNV}-${f}"
    done)
"

LICENCES="icu"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES=""

WORK="${WORKBASE}"

src_unpack() {
    for f in "${EMOJI_FILES[@]}"; do
        edo cp "${FETCHEDDIR}"/${PNV}-${f} "${WORK}"/${f}
    done
}

src_install() {
    insinto /usr/share/unicode/emoji
    doins *
}

